<Query Kind="Program" />

void Main()
{
	CompareParse();
}

// You can define other methods, fields, classes and namespaces here

private static void CompareParse()
{
	const int j = 1_000_000;


	var sw = new Stopwatch();
	var i = 0;
	Console.WriteLine("С try catch");
	sw.Start();

	while (i < j)
	{
		i++;
		try
		{
			var s = int.Parse("s");
			Console.WriteLine(s);
		}
		catch (FormatException)
		{

		}
	}
	sw.Stop();
	Console.WriteLine(sw.ElapsedMilliseconds);
	sw.Reset();
	Console.WriteLine("Без try catch");
	i = 0;
	sw.Start();
	while (i < j)
	{
		try
		{
			i++;
			if (int.TryParse("s", out var s))
			{
				Console.WriteLine(s);
			}
		}
		catch
		{

		}
	}
	sw.Stop();
	Console.WriteLine(sw.ElapsedMilliseconds);
	sw.Reset();
}