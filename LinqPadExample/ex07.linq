<Query Kind="Program" />

void Main()
{
	Demo();
}

// You can define other methods, fields, classes and namespaces here

/// <summary>
/// Демо
/// </summary>
public static void Demo()
{
	// Массив для получения элементов
	int[] array = new int[] { 1, 2, 3, 4, 5 };


	// Получение нулевого элемента массива
	Console.WriteLine(GetItem(array, 0));

	// Получение минус первого элемента (Исключение)

	Console.WriteLine(GetItem(array, -1));

	// получение 11 элемента массив (Исключение)
	Console.WriteLine(GetItem(array, 11));

	// Получение 11 элемента из нула (Исключение)
	Console.WriteLine(GetItem(null, 11));
}



/// <summary>
/// Получение элемента под индексом из массива
/// </summary>
/// <param name="arr"></param>
/// <param name="index"></param>
/// <returns></returns>
static int GetItem(int[] arr, int index)
{

	// Пытается получить элемент массив
	try
	{
		Console.WriteLine();
		Console.WriteLine($"array={(arr == null ? "[]" : $"[{string.Join(", ", arr)}]")} index={index}");
		return arr[index];
	}
	// Если ушли за границу массива   И индекс меньше 0 (напрмиер -1)
	catch (IndexOutOfRangeException) when (index < 0)
	{
		// обрабатываем так
		Console.WriteLine("Индекс меньше ноля");
	}
	// Если ушли за границу массива   И индекс меньше 0 (напрмиер -1)
	catch (IndexOutOfRangeException)
	{
		// обрабатываем так
		Console.WriteLine("Out of range");
	}

	// Если получили другую ошибку (NullReferenceException)
	catch
	{
		Console.WriteLine("Другая ошибка");
	}
	return 0;
}