<Query Kind="Program" />

void Main()
{
	DemoCure();
}

// You can define other methods, fields, classes and namespaces here

/// <summary>
/// Обобшенный класс исключение-болезнь
/// </summary>
class IllnessException : Exception { }

class NoWaterException : Exception { }

/// <summary>
/// Частный случай исключения-болезни (IllnessException)
/// Микробная болезнь
/// </summary>
class MicrobeException : IllnessException { }


/// <summary>
/// Частный случай исключения-болезни (IllnessException)
/// Вирусная болезнь
/// </summary>
class VirusException : IllnessException { }


/// <summary>
///  Функция обработки исключений-болезни
/// </summary>
static void DemoCure()
{
	try
	{
		Live();
	}
	catch (MicrobeException)
	{
		Console.WriteLine("Microb desease");
	}
	// Любое вирусное исключение обрабатываем тут
	catch (VirusException)
	{
		Console.WriteLine("Лечу вирус противовирусными");
	}
	// Все исключения, наследуемые от IllnessException (В нашем случае - MicrobeException)
	// Обрабатываем тут
	//
	// КРОМЕ VirusException, его мы обработали выше
	catch (IllnessException)
	{
		Console.WriteLine("Лечу болезнь лекарствами");
	}
	// Все ОСТАЛЬНЫЕ исключения
	// обрабываем тут
	catch (Exception e)
	{
		Console.WriteLine("Организм справится сам");
	}

}

/// <summary>
/// Функция Жить :)
/// </summary>
static void Live()
{
	//throw new NullReferenceException();
	// Можно так же проверить (расскоментировать следующие две строки
	// throw new VirusException();

	// Или эту ошибку
	 throw new MicrobeException();

}