<Query Kind="Program" />

void Main()
{	
	Example.ShowSimpleException();
	//Example.ShowSimpleExceptionWithMessage();
	//Example.ShowCustomException();
	//Example.ShowCustomExceptionAnother();
	
	Console.WriteLine("Program has been executed succesfully");
	
}

// You can define other methods, fields, classes and namespaces here

class OtusException : Exception
{
	public OtusException() : base()
	{ }

	public OtusException(string message) : base(message)
	{ }
}

public static class Example
{
	public static void ShowSimpleException()=>throw new Exception(); 
	
	public static void ShowSimpleExceptionWithMessage()=>throw new Exception("Something go wrong");
	
	public static void ShowCustomException()=>throw new OtusException("My custom exception has been thrown");
	
	public static void ShowCustomExceptionAnother(){
		var f = new OtusException("Otus ex has been thrown");
		f.Data.Add("some useful information", new {field1=1,fiel2="something about field 2"});
		throw f;		
		//throw new object():
	}
}