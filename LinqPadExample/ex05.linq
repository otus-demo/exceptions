<Query Kind="Program" />

void Main()
{
	//AppDomain.CurrentDomain.UnhandledException+=ExceptionHandler;
	try
	{
		Console.WriteLine("Some activity there");
		throw new Exception("Good bye!");
	}
	catch{
		Console.WriteLine("Unhandled Exception processor");
	}
	finally{
		Console.WriteLine("Release resourses block");
	}
	
	//Console.Writeline("I can reach this point");
}

/// See AppDomain.CurrentDomain.UnhandledException in VS

// You can define other methods, fields, classes and namespaces here

void ExceptionHandler(object sender, UnhandledExceptionEventArgs s)
{
	var ex=(Exception)s.ExceptionObject;
	Console.WriteLine("Unhandled exception had happen! "+ex.Message);
}