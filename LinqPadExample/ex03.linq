<Query Kind="Program" />

//using System.Diagnostics;
void Main()
{
	StackTrace st= new StackTrace(true);
	Console.WriteLine(st);
	A();
}

// You can define other methods, fields, classes and namespaces here

void A() 
{
	Console.WriteLine(Environment.StackTrace);
	B();	
}

void B()
{
	C();
}

void C()
{
	throw new Exception();
}