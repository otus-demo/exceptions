<Query Kind="Program" />

void Main()
{
	try{
		A();
	}
	catch(Exception e){
		Console.WriteLine(e);
	}

	try
	{
		A2();
	}
	catch (Exception e)
	{
		Console.WriteLine(e);
	}
}

// You can define other methods, fields, classes and namespaces here

static void A()
{
	B();
}

static void B()
{
	try
	{
		C();
	}
	catch (Exception e)
	{
		Console.WriteLine("B Ошибка");
		e.Data.Add("Some Info",1);
		throw;
	}
}

static void A2()
{
	B2();
}


static void B2()
{
	try
	{
		C();
	}
	catch (Exception e)
	{
		Console.WriteLine("B2 Ошибка");
		throw new Exception("B2");
	}
}

static void C()
{
	throw new Exception();
}