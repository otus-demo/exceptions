﻿using System.Diagnostics;

class OtusException : Exception
{
    public OtusException() : base()
    { }

    public OtusException(string message) : base(message)
    { }
}


class FabulousException : Exception
{
    public FabulousException() : base()
    { }

    public FabulousException(string message) : base(message)
    { }
}


class ExtraException : OtusException
{
    public ExtraException() : base()
    { }

    public ExtraException(string message) : base(message)
    { }
}





class Program
{


    static void Level1()
    {
        Level2();


    }



    static void Level2()
    {
        try
        {
            LevelException();
        }
        catch (Exception e)
        {
            Console.WriteLine(@$"Level2: {e.Message}");
            throw;
        }


    }


    static void LevelException()
    {
        throw new FabulousException("Я просто ошибка");

    }

    static void ThrowExcFun()
    {
        try
        {
            Console.WriteLine();
            Console.WriteLine("Я тут");
            Console.WriteLine();
            int.Parse(Console.ReadLine());

        }
        catch (FormatException excccccc)
        {
            Console.WriteLine($"Ошибка: {excccccc.Message}");
            throw;
        }
        catch (OverflowException excc)
        {
            Console.WriteLine($"ОВЕРФЛОУ: {excc.Message}");
        }

    }

    static void Foo()
    {
        Foo();
    }


    static void FailFastEx()
    {
        try
        {
            Console.WriteLine("Я в программе");
            Environment.FailFast("Экстренно падаю");
            Console.WriteLine("Я все еще в программе");
        }
        catch (Exception e)
        {
            Console.WriteLine("Я поймал ошибку");
        }
        finally
        {
            Console.WriteLine("Я в finally");
        }
    }

    static async Task Main(string[] args)
    {
        //AppDomain.CurrentDomain.UnhandledException += ExceptionHandler;
        //Console.WriteLine("Some activity there");
        //throw new Exception("Good bye!");

        //FailFastEx();

        //SimpleDivideByZeroException();

        //try 
        //{
        //    UnCatchableException();
        //}
        //catch (Exception e)
        //{
        //    Console.WriteLine("process uncatchable exception");
        //    Console.WriteLine(e);
        //}
        //finally { Console.WriteLine("after uncatchable exception"); }


        //CompareParse();


        //var t = Task.Run(() =>
        //{
        //    throw new Exception("Task failed1");
        //});

        //var t2 = Task.Run(() =>
        //{
        //    throw new Exception("Task failed2");
        //});

        //try
        //{
        //    //await 
        //   Task.WhenAll(t,t2);

        //}
        //catch (AggregateException e)
        //{
        //    foreach (var ex in e.InnerExceptions)
        //    {
        //        Console.WriteLine("Внутрення ошибка" + ex.Message);
        //    }
        //}
        //catch (Exception e)
        //{
        //    Console.WriteLine(e.Message + ' ' + e.GetType());
        //}


       // Execute1Async();

       // Execute2Async();

        Execute3Async();



        //A();
        #region FailFast

        #endregion


        //CompareParse();
        //InternalExceptions();


        //	A();
    }

    private static void FailedFun(object sender, UnhandledExceptionEventArgs e)
    {
        var ex = (Exception)e.ExceptionObject;

        Console.WriteLine($"Ошибка: {ex.Message}");
    }

    public class FunResult
    {
        public bool Ok { get; set; }
        public string ErrorMEssage { get; set; }

        public int? Data { get; set; }
    }

    public static FunResult Parse(string s)
    {

        if (int.TryParse(s, out var num))
        {
            return new FunResult { Ok = true, Data = num };
        }
        else
        {
            return new FunResult { Ok = false, ErrorMEssage = "Неправильная строка" };
        }
    }


    private static void CompareParse()
    {
        const int j = 1_000;


        var sw = new Stopwatch();
        var i = 0;
        Console.WriteLine("С try catch");
        sw.Start();

        while (i < j)
        {
            i++;
            try
            {
                var s = int.Parse("s");
                Console.WriteLine(s);
            }
            catch (FormatException)
            {

            }
        }
        sw.Stop();
        Console.WriteLine(sw.ElapsedMilliseconds);
        sw.Reset();
        Console.WriteLine("Без try catch");
        i = 0;
        sw.Start();
        while (i < j)
        {
            try
            {
                i++;
                if (int.TryParse("s", out var s))
                {
                    Console.WriteLine(s);
                }
            }
            catch
            {

            }
        }
        sw.Stop();
        Console.WriteLine(sw.ElapsedMilliseconds);
        sw.Reset();
    }



    /// <summary>
    /// Обработчик ошибки перед завершение программы
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private static void ExceptionHandler(object sender, UnhandledExceptionEventArgs e)
    {
        if (e.ExceptionObject is OtusException)
        {
            Console.WriteLine(":(");
        }
        Console.WriteLine("Я завершился ненормально");

    }

    #region MyRegion
    static void A()
    {

        B();

    }

    static void B()
    {
        try
        {
            C();
        }
        catch (Exception e)
        {
            Console.WriteLine("B Ошибка");
            throw;
        }
    }

    static void C()
    {
        throw new Exception();
    }

    #endregion


    #region Условные исключени



    /// <summary>
    /// Демо
    /// </summary>
    public static void Demo()
    {
        // Массив для получения элементов
        int[] array = new int[] { 1, 2, 3, 4, 5 };


        // Получение нулевого элемента массива
        Console.WriteLine(GetItem(array, 0));

        // Получение минус первого элемента (Исключение)

        Console.WriteLine(GetItem(array, -1));

        // получение 11 элемента массив (Исключение)
        Console.WriteLine(GetItem(array, 11));

        // Получение 11 элемента из нула (Исключение)
        Console.WriteLine(GetItem(null, 11));
    }



    /// <summary>
    /// Получение элемента под индексом из массива
    /// </summary>
    /// <param name="arr"></param>
    /// <param name="index"></param>
    /// <returns></returns>
    static int GetItem(int[] arr, int index)
    {

        // Пытается получить элемент массив
        try
        {
            Console.WriteLine();
            Console.WriteLine($"array={(arr == null ? "[]" : $"[{string.Join(", ", arr)}]")} index={index}");
            return arr[index];

        }

        // Если ушли за границу массива   И индекс меньше 0 (напрмиер -1)
        catch (IndexOutOfRangeException) when (index < 0)
        {
            // обрабатываем так
            Console.WriteLine("Индекс меньше ноля");
        }
        // Если ушли за границу массива   И индекс меньше 0 (напрмиер -1)
        catch (IndexOutOfRangeException)
        {
            // обрабатываем так
            Console.WriteLine("Out of range");
        }

        // Если получили другую ошибку (NullReferenceException)
        catch
        {
            Console.WriteLine("Другая ошибка");
        }
        return 0;
    }

    #endregion



    #region Перехваты исключений внутри


    static void InternalExceptions()
    {
        try
        {
            try
            {
                int.Parse(Console.ReadLine());
            }
            catch (FormatException e)
            {
                Console.WriteLine($"Внутри: {e.Message}");
            }
        }
        catch (Exception e)
        {
            Console.WriteLine($"Снаружи: {e.Message}");
        }
    }
    #endregion


    #region Illness

    /// <summary>
    /// Обобшенный класс исключение-болезнь
    /// </summary>
    class IllnessException : Exception { }

    class NoWaterException : Exception { }

    /// <summary>
    /// Частный случай исключения-болезни (IllnessException)
    /// Микробная болезнь
    /// </summary>
    class MicrobeException : IllnessException { }


    /// <summary>
    /// Частный случай исключения-болезни (IllnessException)
    /// Вирусная болезнь
    /// </summary>
    class VirusException : IllnessException { }


    /// <summary>
    ///  Функция обработки исключений-болезни
    /// </summary>
    static void DemoCure()
    {
        try
        {
            Live();
        }
        // Любое вирусное исключение обрабатываем тут
        //catch (MicrobeException)
        //{
        //	Console.WriteLine("Лечу вирус противовирусными");
        //}
        catch (VirusException)
        {
            Console.WriteLine("Лечу вирус противовирусными");
        }

        // Все исключения, наследуемые от IllnessException (В нашем случае - MicrobeException)
        // Обрабатываем тут
        //
        // КРОМЕ VirusException, его мы обработали выше
        catch (IllnessException)
        {
            Console.WriteLine("Лечу болезнь лекарствами");
        }
        // Все ОСТАЛЬНЫЕ исключения
        // обрабываем тут
        catch (Exception e)
        {
            Console.WriteLine("Организм справится сам");
        }

    }

    /// <summary>
    /// Функция Жить :)
    /// </summary>
    static void Live()
    {
        throw new NullReferenceException();
        // Можно так же проверить (расскоментировать следующие две строки

        // Или эту ошибку
        // throw new MicrobeException();

    }
    #endregion

    static void SimpleDivideByZeroException() 
    {
        var a = 2;
        var b = 0;
        var c = a/b;
    }

    static void UnCatchableException()
    {
        throw new AccessViolationException();
    }


    private static async Task ThrowInAsync()
    {
        throw new InvalidOperationException("Task finished with exception");
    }

    public static async Task Execute1Async()
    {
        Task task = null;
        try
        {
            task = ThrowInAsync();
            await task;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }

    }

    public static async Task Execute2Async()
    {
        Task allTasks = Task.CompletedTask;
        try
        {
            var task1 = ThrowInAsync();
            var task2 = ThrowInAsync();
            var task3 = ThrowInAsync();

            allTasks=Task.WhenAll( task1, task2, task3);

            await allTasks;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
            var i = 0;
            foreach (Exception e in allTasks.Exception.InnerExceptions)
            {
                i++;    
                Console.WriteLine(i +" - " + e.ToString());
            }

        }

    }

    public static async Task Execute3Async()
    {
        Task<bool> task=null;
        try
        {
            task = Task.Run<bool>(async () =>
            {
                await ThrowInAsync();
                return true;
            });
            var result = task.Result;
            
            //task.Wait();
            //var result=task.GetAwaiter().GetResult();
        }
        catch (AggregateException ex)
        {
            foreach (Exception exception in ex.InnerExceptions)
            {
                Console.WriteLine(exception.ToString());
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());

        }

    }

}
